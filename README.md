# AberraEtAl2018Refactor
This code is a refactorization of Aberra et al. 2018 (https://modeldb.science/241165) to allow a use to sweep through stimulation settings using code.

The code takes in a .dat file containing the voltages at each section of the model.

The pulse width is set by setting the pw variable.

The code is run by running the RunCorticalNeuronSims.py file.

## Citing
This work is completely based off of Aberra et al. 2018, so if you are citing this work please cite Aberra et al. 2018.

Aberra AS, Peterchev AV, Grill WM. (2018). Biophysically realistic neuron models for simulation of cortical stimulation. Journal of neural engineering. 15 [PubMed]
