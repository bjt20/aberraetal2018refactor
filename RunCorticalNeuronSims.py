import os
#os.chdir("C:/Users/bjt20/Downloads/241165/AberraEtAl2018")
os.chdir("D:/Motif/AberraEtAl2018")
# cellIds = [6,7,8,9,10,16,17,18,19,20]
# pws = [0.02,0.05,0.1,0.2,0.5,1]
cellIds = [20]
pws = [0.05]
for Id in cellIds:
	for pw in pws:
		cmd = f"neuron -NSTACK 100000 -NFRAME 20000 -c cellInd={Id} -c pw={pw} init.hoc -c quit()"
		os.system(cmd)